features and use-cases
**********************

the portions of this namespace are simplifying your Python application or service in the areas/domains::

    * continuous integration
    * continuous deployment
    * outsourced text file (maintained centrally)
    * code and config file templates
    * git repository management (locally and remotely)
    * software development
    * software testing and QA

