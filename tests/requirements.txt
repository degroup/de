# THIS FILE IS EXCLUSIVELY MAINTAINED by the project de_tpl_project V0.2.3 
anybadge
coverage-badge
de_git_repo_manager    # including Pillow, ae_base, ae_files, ae_paths for test of img/loc/snd resources
flake8
mypy
pylint
pytest
pytest-cov
